'use strict';

angular.module('jelatynaSApp')
    .controller('HeaderCtrl', function ($scope, $location) {
        $scope.isPlaying = false;
        $scope.play = function () {
            $scope.isPlaying = true;
        };
        $scope.isOnMainPage = function () {
            return $location.url() === "/";
        };
    });
