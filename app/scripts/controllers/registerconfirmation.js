'use strict';

angular.module('jelatynaSApp')
    .controller('RegisterConfirmationCtrl', function ($scope, Confirmation, $routeParams) {
        $scope.loading = true;
        $scope.success = true;
        var promise = Confirmation.save({"action": "confirm", "token": $routeParams.token}).$promise;
        promise.catch(function () {
            $scope.success = false;
        });
        promise['finally'](function () {
            $scope.loading = false;
        });
    });
