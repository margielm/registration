'use strict';

angular.module('jelatynaSApp')
    .directive('page', ['Pages', function (Pages) {
        return {
            scope: {name: "@"},
            template: '<div ng-bind-html="page.text"></div>',
            restrict: 'E',
            replace: true,
            controller: function ($scope) {
                $scope.page = Pages.get({"name": $scope.name});
            }

        };
    }]);
