'use strict';

angular.module('jelatynaSApp')
    .directive('menu', function () {
        return {
            templateUrl: '/views/directives/menu.html',
            restrict: 'E',
            replace: true,
            controller: function ($scope, $attrs) {
                $scope.classes = "nav nav-justified navbar-collapse collapse"
                if ($attrs['type'] === 'footer'){
                    $scope.classes = "nav navbar-nav navbar-right hidden-xs";
                }
                $scope.items = [
                    {name: "główna", url: "#/"},
                    {name: "O nas", url: "#/about"},
                    {name: "Partnerzy", url: "#/partners"},
                    {name: "Najważniejsze Informacje", url: "#/info"},
                    {name: "Aktualności", url: "#/news"},
                    {name: "Prezentacje", url: "#/presentations"}

                ];
            }
        };
    });
