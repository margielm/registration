'use strict';

angular.module('jelatynaSApp')
    .directive('person', ['$modal', function ($modal) {
        var modal;
        return {
            templateUrl: function (tElement, tAttrs) {
                if (tAttrs.type === 'text') {
                    return '/views/directives/person-text.html'
                } else {
                    return '/views/directives/person.html'
                }
            },
            restrict: 'E',
            replace: true,
            scope: {
                'person': '=for', 'type': '='
            },
            controller: function ($scope) {
                $scope.$on("open", function () {
                    $scope.showModal();
                });
                $scope.showModal = function () {
                    if (modal == null) {
                        modal = $modal.open({
                            templateUrl: '/views/directives/person-modal.html',
                            windowClass: 'person-modal',
                            scope: $scope,
                            backdrop: false,
                            keyboard: true
                        });
                        modal.result['finally'](function () {
                            modal = null
                        });
                    }
                };

                $scope.close = function () {
                    modal.close();
                };

                $scope.photoFor = function (person) {
                    return person.photo || '/images/placeholder.png';
                };
            }
        };
    }])
;
