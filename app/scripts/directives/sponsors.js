'use strict';

angular.module('jelatynaSApp')
    .directive('sponsors', ['Sponsor', function (Sponsor) {
        return {
            templateUrl: '/views/directives/sponsors.html',
            restrict: 'E',
            controller: function ($scope) {
                $scope.sponsors = Sponsor.get(function () {
                    $scope.$broadcast("late-load", "sponsors");
                });
            }
        };
    }]);
