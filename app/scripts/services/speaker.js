'use strict';

angular.module('jelatynaSApp')
    .factory('Speaker', function ($resource) {
        return $resource('/speakers');
    });
