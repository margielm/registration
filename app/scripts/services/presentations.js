'use strict';

angular.module('jelatynaSApp')
    .factory('Presentations', ["$resource", "api-server", function ($resource, apiServer) {
        return $resource(apiServer + '/presentations');
    }]);
