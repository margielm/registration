'use strict';

angular.module('jelatynaSApp')
    .factory('Twitter', ["$resource", "api-server", function ($resource, apiServer) {
        return  $resource(apiServer + "/twitter");
    }]);
