'use strict';

angular.module('jelatynaSApp')
    .factory('Confirmation', ["$resource", "api-server", function ($resource, apiServer) {
        return $resource(apiServer + '/register/:action/:token', {'action': '@action', 'token': '@token'}, {});
    }
    ])
