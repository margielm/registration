'use strict';

angular.module('jelatynaSApp', [
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngRoute',
//    'ngAnimate',
    'ui.bootstrap',
//    'infinite-scroll',
    'cfp.hotkeys'
])
    .constant("api-server", "http://127.0.0.1:9090/api")
    .config(function ($routeProvider) {
        $routeProvider
            .when('/', {templateUrl: 'views/main.html'})
            .when('/about', {templateUrl: 'views/about.html'})
            .when('/info', {templateUrl: 'views/info.html'})
            .when('/news', {templateUrl: 'views/all-news.html'})
            .when('/partners', {templateUrl: 'views/partners.html'})
            .when('/v4p/', {templateUrl: 'views/v4p/v4p.html'})
            .when('/v4p/:key', {templateUrl: 'views/v4p/v4p.html'})
            .when('/register', {templateUrl: 'views/register.html'})
            .when('/presentations', {templateUrl: 'views/presentations.html'})
            .when('/register/confirm/:token', {templateUrl: 'views/register-confirm.html'})
            .otherwise({redirectTo: '/'});
    });
