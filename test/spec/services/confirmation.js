'use strict';

describe('Service: confirmation', function () {

  // load the service's module
  beforeEach(module('jelatynaSApp'));

  // instantiate service
  var confirmation;
  beforeEach(inject(function (_confirmation_) {
    confirmation = _confirmation_;
  }));

  it('should do something', function () {
    expect(!!confirmation).toBe(true);
  });

});
