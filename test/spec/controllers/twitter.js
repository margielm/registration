'use strict';

describe('Controller: TwitterCtrl', function () {

    beforeEach(module('jelatynaSApp'));

    var TwitterCtrl, scope, twitter, deferred;

    beforeEach(inject(function ($controller, $rootScope, _$q_) {
        scope = $rootScope.$new();
        deferred = _$q_.defer();
        twitter = {
            query: function () {
                return {'$promise': deferred.promise};
            }
        };
        TwitterCtrl = $controller('TwitterCtrl', {
            $scope: scope,
            Twitter: twitter
        });
    }));

    it('set first tweet', function () {
        var tweet = aTweet("my tweet");
        deferred.resolve([
            tweet
        ]);
        scope.$apply();

        expect(scope.tweet).toBe(tweet);
    });


    it("replace one hashtag with url", function () {
        var tweet = aTweet("#tag", [
            {text: "tag"}
        ]);
        deferred.resolve([
            tweet
        ]);
        scope.$apply();

        expect(scope.tweet.text).toEqual(urlTo("tag"))
    });


    it("replace two hashtags with url", function () {
        var tweet = aTweet("#tag1 and #tag2", [
            {text: "tag1"},
            {text: "tag2"}
        ]);
        deferred.resolve([
            tweet
        ]);
        scope.$apply();

        expect(scope.tweet.text).toEqual(urlTo("tag1") + " and "+urlTo("tag2"))
    });

    function aTweet(text, hashtags) {
        hashtags = hashtags || [];
        var tweet = {
            text: text,
            entities: {
                hashtags: hashtags}
        };
        return tweet;
    }

    function urlTo(tag) {
        return "<a href='https://twitter.com/hashtag/:tag' target='_blank'>#:tag</a>".replace(/:tag/g, tag);
    }

});
