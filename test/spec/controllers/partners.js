'use strict';

describe('Controller: PartnersCtrl should', function () {

    // load the controller's module
    beforeEach(module('jelatynaSApp'));

    var PartnersCtrl,
        scope;

    // Initialize the controller and a mock scope
    beforeEach(inject(function ($controller, $rootScope) {
        scope = $rootScope.$new();
        PartnersCtrl = $controller('PartnersCtrl', {
            $scope: scope
        });
    }));


    it("hide type if not available", function () {
        scope.partners = {};

        var visible = scope.isVisible(goldType());

        expect(visible).toBeFalsy();
    });

    it("show type if has partners", function () {
        scope.partners = {"gold": [
            {id: 1}
        ]};

        var visible = scope.isVisible(goldType());

        expect(visible).toBeTruthy();
    });

    it("hide type if available but without partners", function () {
        scope.partners = {"gold": []};

        var visible = scope.isVisible(goldType());

        expect(visible).toBeFalsy();
    });

    function goldType() {
        return {id: "gold"};
    }
});
