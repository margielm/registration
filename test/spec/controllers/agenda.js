'use strict'

describe('Controller: AgendaCtrl Should', function () {

    beforeEach(module('jelatynaSApp'));

    var AgendaCtrl = {};
    var scope = {};

    beforeEach(inject(function ($controller, $rootScope, _Agenda_) {
        scope = $rootScope.$new();
        spyOn(_Agenda_, "get").and.returnValue({visibleRooms: []});
        AgendaCtrl = $controller('AgendaCtrl', {
            $scope: scope,
            Agenda: _Agenda_
        })
    }));


    it('get only presentation', function () {
        var presentation = {title: "presentation"};
        withVisibleRooms("A");
        withSchedule({
            slotId: 1, presentations: [
                { room: "A", presentation: presentation }
            ]
        });

        var presentations = scope.getAllPresentationsForSlot(1);

        expect(presentations).toEqual([presentation]);
    });

    it('get presentation for second slot', function () {
        var presentation = {title: "presentation"};
        withVisibleRooms("A");
        withSchedule({
            slotId: 1, presentations: [
                { room: "A", presentation: {} }
            ]
        }, {
            slotId: 2, presentations: [
                { room: "A", presentation: presentation }
            ]
        });

        var presentations = scope.getAllPresentationsForSlot(2);

        expect(presentations).toEqual([presentation]);
    });


    it('get presentations from ALL rooms', function () {
        var presentation1 = {title: "presentation1"};
        var presentation2 = {title: "presentation2"};
        withVisibleRooms("A", "B");
        withSchedule(
            { slotId: 1, presentations: [
                { room: "A", presentation: presentation1 },
                { room: "B", presentation: presentation2 }
            ]
            });

        var presentations = scope.getAllPresentationsForSlot(1);

        expect(presentations).toEqual([presentation1, presentation2]);
    })

    it('get presentations for visible rooms only', function () {
        var presentation1 = {title: "presentation1"};
        withVisibleRooms("A");
        withSchedule(
            {   slotId: 1, presentations: [
                { room: "A", presentation: presentation1 },
                { room: "B", presentation: {} }
            ]
            });

        var presentations = scope.getAllPresentationsForSlot(1);

        expect(presentations).toEqual([presentation1]);
    });


    describe("Rooms should", function () {
        it("be added to visible list", function () {
            withRooms(["A"]);

            scope.changed("A", true);

            expect(scope.agenda.visibleRooms).toEqual(["A"]);
        });


        it("be removed from visible list", function () {
            withRooms(["A"])
            scope.changed("A", true);

            scope.changed("A", false);

            expect(scope.visibleRooms).toEqual([]);
        });

        it("be sorted by given order", function () {
            var rooms = ["B", "C", "A"];
            withRooms(rooms);

            scope.changed("C", true);
            scope.changed("B", true);
            scope.changed("A", true);

            expect(scope.agenda.visibleRooms).toEqual(rooms);
        });

        it("be visible if on the list", function () {
            withVisibleRooms("A");

            var visible = scope.isVisible("A");

            expect(visible).toBeTruthy();
        });

        it("not be visible if not on the list", function () {
            withVisibleRooms("A");

            var visible = scope.isVisible("B");

            expect(visible).toBeFalsy();
        });
    });


    function withRooms(rooms) {
        scope.agenda.rooms = rooms;
    }

    function withSchedule(schedule) {
        scope.agenda.schedule = _.toArray(arguments);
    }

    function withVisibleRooms(rooms) {
        scope.agenda.visibleRooms = _.toArray(arguments);
    }

});